dc_run := docker-compose run --rm -p 8055:8055 larch-core

build:
	docker-compose build

run:
	$(dc_run)

bash:
	$(dc_run) bash
