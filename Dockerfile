FROM golang:1.11

WORKDIR /go/src/app
COPY . .

RUN go get github.com/codegangsta/gin

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["gin", "-p", "8055", "run", "main.go"]
